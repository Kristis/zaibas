   var ctx = document.getElementById("myChart");
   var myChart = new Chart(ctx, {
    type: 'bar', 
    data: {
        labels: ["Benzinas A95", "Benzinas A98", "Dyzelinas", "Dujos"],
        datasets: [{
            data: [1.22, 1.19, 1.11, 0.54],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)'
                            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        title: {
          display: true,
          text: 'Degalų kainos 2018-07-01',
          fontSize: 20
        },
        legend: {
          display: false
        },
        scales: {
            xAxes: [
            {
              ticks: {
                beginAtZero:true,
                fontSize: 16
              }
            }],
            yAxes: [
            {
                ticks: {
                    beginAtZero:true,
                    fontSize: 16
                }
            }]
        }
      }
  });

