 //<!-- Grafikas pyragas -->
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
google.charts.setOnLoadCallback(drawChart2);
google.charts.setOnLoadCallback(drawChart3);
google.charts.setOnLoadCallback(drawChart4);

// Draw the chart and set the chart values
function drawChart2() {
  var data = google.visualization.arrayToDataTable([
  ['', ''],
  ['Dyzelis', 0],
  ['Benzinas', 24000],
  ['Dujos', 0],
  ['Biodegalai', 0],
  ['Elektra', 10000],
  ['Kita', 6000]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Energijos šaltinių sąnaudos', 'width':550, 'height':400, 'is3D': true, 'titleTextStyle':{fontSize: '18',}};

  // Display the chart inside the <div> element with id="piechart2"
  var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));
  chart2.draw(data, options);
}


function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Naudojamas kuras', ''],
  ['Muscles power', 5000],
  ['Benzinas', 200],
  ['Saulės energija',1000],
  ['Vegan power', 3200],
  ['Elektra', 1500],
  ['Pozityvumas', 7000]
]);
  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Degalų sąnaudos', 'width':550, 'height':400, 'is3D': true, 'titleTextStyle':{fontSize: '18',}};
  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}

function drawChart3() {
  var data = google.visualization.arrayToDataTable([
  ['Dyzelino kainos sandara', ''],
  ['Dyzelino kaina iki mokesčių', 48],
  ['Mokesčiai', 44],
  ['Degalinės marža', 8]
  ]);
  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Dyzelino kainos sandara, %', 'width':550, 'height':400, 'is3D': true, 'titleTextStyle':{fontSize: '18',}};
  // Display the chart inside the <div> element with id="piechart3"
  var chart3 = new google.visualization.PieChart(document.getElementById('piechart3'));
  chart3.draw(data, options);
}

function drawChart4() {
  var data = google.visualization.arrayToDataTable([
  ['Degalų sąnaudos', ''],
  ['Dyzelis', 0],
  ['Benzinas', 100],
  ['Dujos', 0],
  ['Biodegalai', 0],
  ['Elektra', 100],
  ['Kita', 100]
]);
  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Degalų sąnaudos', 'width':550, 'height':400, 'is3D': true, 'titleTextStyle':{fontSize: '18',}};
  // Display the chart inside the <div> element with id="piechart4"
  var chart4 = new google.visualization.PieChart(document.getElementById('piechart4'));
  chart4.draw(data, options);
}

